{-# language TupleSections #-}
{-# language ViewPatterns #-}
{-# language FlexibleContexts #-}
{-# language OverloadedStrings #-}
{-# language LambdaCase #-}
{-# language BangPatterns #-}

module Main where

import Control.Concurrent
import Control.Exception hiding (handle)
import Control.Monad
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import qualified Data.ByteString.Lazy as BSL
import Data.ByteString (ByteString)
import Data.Functor
import Data.Maybe
import Foreign.C.Error
import Foreign.C.String
import Foreign.C.Types
import Network.Socket
import Network.Socket.ByteString
import System.Environment
import System.Directory(doesDirectoryExist)
import System.IO(hPrint,stderr)
import System.IO.Error
import System.Posix.User
import Text.Read

foreign import ccall unsafe "chroot" c_chroot :: CString -> IO Int

chroot = flip withCString $ \cfp -> throwErrnoIfMinus1_ "chroot" (c_chroot cfp)

readE msg a =
  case readMaybe a of
    Just x -> x
    Nothing -> "Prelude.read: no parse: " <> msg

getEnvs keys = do
  ls <- traverse (\k -> (k,) <$> lookupEnv k) keys
  let missingKeys = [k | (k, Nothing) <- ls]
  let presentValues = [v | (_, Just v) <- ls]
  return $
    if null missingKeys then
      presentValues
    else
      error $ "environment variables missing: " <> show missingKeys

main :: IO ()
main = do
  [read -> !uid, read -> !gid] <- getEnvs ["UID", "GID"]
  host <- lookupEnv "HHTTPD_HOST"
  port <- fromMaybe "80" <$> lookupEnv "HHTTPD_PORT"
  chroot . head =<< getArgs
  let hints = defaultHints { addrFlags = [AI_PASSIVE], addrSocketType = Stream }
  addr <- head <$> getAddrInfo (Just hints) host (Just port)
  bracket (open addr) close (run uid gid)
  where
  open addr = bracketOnError (openSocket addr) close $ \sock -> do
    setSocketOption sock ReuseAddr 1
    withFdSocket sock setCloseOnExecIfNeeded
    bind sock $ addrAddress addr
    listen sock 1024
    return sock

run uid gid sock = do
  setEffectiveGroupID gid
  setGroupID gid
  setUserID uid
  setEffectiveUserID uid
  forever $
    bracketOnError (accept sock) (close . fst) $ \(client,_) ->
    forkFinally (handle client) (const $ gracefulClose client 500)

handle client = do
  -- \r character 13
  methodLine <- BS.takeWhile (/= 13) <$> recv client 512
  -- space character 32
  let ~[method, BSC.unpack -> uri, _] = BS.split 32 methodLine
  sendMany client =<<
    case method of
      "GET" | null uri ->
        pure (redirectToIndex uri)
      "GET" | last uri == '/' ->
        getIndex uri
      "GET" -> do
        isDir <- doesDirectoryExist uri
        if isDir then
          pure (redirectToIndex uri)
        else
          getFile uri
      _ ->
        pure [wrongMethodLine]
  where
  okMethodLine = "HTTP/1.1 200 OK\r\n\r\n"
  wrongMethodLine = "HTTP/1.1 405 BROKE\r\n\r\n"
  notFoundMethodLine = "HTTP/1.1 404 NOT FOUND\r\n\r\n"
  getFile uri =
    try (BSL.readFile uri) >>= \case
      Left (ioeGetErrorType -> errty) | isDoesNotExistErrorType errty ->
        pure [notFoundMethodLine]
      Left ex -> do
        hPrint stderr ex
        pure []
      Right fc ->
        pure $ okMethodLine : BSL.toChunks fc
  getIndex uri = getFile (uri ++ "/index.html")
  redirectToIndex uri =
    ["HTTP/1.1 301 MOVED PERMANENTLY\r\nLocation: ", BSC.pack (uri ++ "/"), "\r\n\r\n"]
